# Intro
This is example code for implamenting the MultiLevelPushMenu menu in a custom Drupal 8 theme.
https://www.jqueryscript.net/menu/Multi-Level-Slide-Push-Menu-with-CSS3-Javascript-MultiLevelPushMenu.html

# Instructions
- The example files only include the code needed for the menu, so you can't simply replace your theme files with them.
- The *customtheme* name will need to be replaced with you custom theme everywhere it's used.